package com.peterpig.codehome.controller;

import com.peterpig.codehome.pojo.Admin;
import com.peterpig.codehome.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * 管理员控制层
 * @Author: Evan
 * @Date: 2019-1-11 16:25
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @RequestMapping("/login/{loginName}/{loginPassword}")
    @ResponseBody
    public Admin login(Admin admin, HttpSession session) {
        Admin result = adminService.login(admin);
        if (result != null) {
            session.setAttribute("curAdmin", result);
        }
        return result;
    }

    @RequestMapping("/selectAdmin/{id}")
    @ResponseBody
    public Admin selectAdmin(@PathVariable Integer id) {
        Admin admin = adminService.selectById(id);
        return admin;
    }

    @RequestMapping("/addAdmin/{loginName}/{loginPassword}")
    @ResponseBody
    public int addAdmin(Admin admin) {
        admin.setCreateTime(new Date());
        admin.setStatus(true);
        int result = adminService.addAdmin(admin);
        if (result == 1) {
            return 1;
        }
        return 0;
    }

    @RequestMapping("/updateAdmin/{id}/{loginName}/{loginPassword}/{status}/{lastLoginTime}/{createTime}")
    @ResponseBody
    public int updateAdmin(Admin admin) {
        int result = adminService.updateAdmin(admin);
        if (result == 1) {
            return 1;
        }
        return 0;
    }
}
