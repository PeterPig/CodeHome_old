package com.peterpig.codehome.controller;

import com.peterpig.codehome.pojo.User;
import com.peterpig.codehome.service.UserService;
import com.peterpig.codehome.utils.CommonReturnType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value="/login/{loginName}/{loginPassword}",method = RequestMethod.POST)
    @ResponseBody
    public String login(User u, HttpSession session, Model model){
        System.out.println(u.getLoginName());
        User user=this.userService.login(u);
        if(user!=null){
            session.setAttribute("User",user);
        }else{
            model.addAttribute("msg","用户名或密码错误!");
        }
        return "200";
    }

    @RequestMapping("/register")
    public String register(User u,Model model){
        //注册
        int result=this.userService.register(u);
        if(result==1){
            model.addAttribute("注册成功","msg");
            return "index";
        }
        model.addAttribute("注册失败！","msg");
        return "index";
    }

    @RequestMapping("/updateNickname/{userId}/{nickname}")
    @ResponseBody
    public CommonReturnType updateNickname(@PathVariable("userId") Integer userId,@PathVariable("nickname") String nickname){
        CommonReturnType commonReturnType=new CommonReturnType();
        int result=this.userService.updateNickname(userId,nickname);
        commonReturnType.setResult(result);
        return commonReturnType;
    }
}
