package com.peterpig.codehome.controller;

import com.peterpig.codehome.pojo.UserInfo;
import com.peterpig.codehome.service.UserInfoService;
import com.peterpig.codehome.utils.CommonReturnType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: jakciel
 * @Date: 2019/1/12 14:55
 * @Version 1.0
 */
@Controller
@RequestMapping("/user/userInfo")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    @RequestMapping("/addUserInfo/{userId}/{realName}/{bornDate}/{gender}/{advantageLang}")
    @ResponseBody
    public CommonReturnType addUserInfo(UserInfo userInfo){
        CommonReturnType commonReturnType=new CommonReturnType();
        int result=this.userInfoService.addUserInfo(userInfo);
        commonReturnType.setResult(result);
        return commonReturnType;
    }

    @RequestMapping("/updateUserInfo/{userId}/{realName}/{bornDate}/{gender}/{advantageLang}")
    @ResponseBody
    public CommonReturnType updateUserInfo(UserInfo userInfo){
        CommonReturnType commonReturnType=new CommonReturnType();
        int result=this.userInfoService.updateUserInfo(userInfo);
        commonReturnType.setResult(result);
        return commonReturnType;
    }
}
