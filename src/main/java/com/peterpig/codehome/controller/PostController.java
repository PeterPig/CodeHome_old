package com.peterpig.codehome.controller;

import com.peterpig.codehome.pojo.Post;
import com.peterpig.codehome.service.PostService;
import com.peterpig.codehome.utils.CommonReturnType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * @Author: jakciel
 * @Date: 2019/1/21 13:37
 * @Version 1.0
 */
@Controller
@RequestMapping("/post")
public class PostController {

    @Autowired
    private PostService postService;

    @RequestMapping("/addPost")
    @ResponseBody
    public CommonReturnType createPost(@RequestBody  Post post){
        post.setStatus(true);
        post.setCreateTime(new Date());
        int result=this.postService.newPost(post);
        CommonReturnType commonReturnType=new CommonReturnType();
        if(result==1){
            commonReturnType.setResult(1);
            return commonReturnType;
        }
        return commonReturnType;
    }
}
