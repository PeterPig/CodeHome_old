package com.peterpig.codehome.service;

import com.peterpig.codehome.pojo.Admin;

/**
 * 管理员服务层接口
 * @Author: Evan
 * @Date: 2019-1-11 15:41
 */
public interface AdminService {
    /**
     * 管理员登录
     * @param admin 管理员对象
     * @return 返回null表示用户不存在，非null则登录成功
     */
    Admin login(Admin admin);

    /**
     * 通过ID查询管理员
     * @param id 管理员ID
     * @return 管理员对象
     */
    Admin selectById(int id);

    /**
     * 添加管理员
     * @param admin 管理员对象
     * @return 返回-1系统错误，返回0添加失败，返回1添加成功
     */
    int addAdmin(Admin admin);

    /**
     * 修改管理员
     * @param admin 管理员对象
     * @return 返回-1系统错误，返回0修改失败，返回1修改成功
     */
    int updateAdmin(Admin admin);
}
