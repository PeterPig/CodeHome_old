package com.peterpig.codehome.service.impl;

import com.peterpig.codehome.pojo.Language;
import com.peterpig.codehome.service.LanguageService;

import java.util.List;

/**
 * @Author: Evan
 * @Date: 2019-1-14 18:07
 */
public class LanguageServiceImpl implements LanguageService {
    @Override
    public int addLang(Language language) {
        return 0;
    }

    @Override
    public int updateLang(Language language) {
        return 0;
    }

    @Override
    public int deleteLang(int id) {
        return 0;
    }

    @Override
    public Language selectById(int id) {
        return null;
    }

    @Override
    public List<Language> selectAll() {
        return null;
    }
}
