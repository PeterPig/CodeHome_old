package com.peterpig.codehome.service.impl;

import com.peterpig.codehome.mapper.UserMapper;
import com.peterpig.codehome.pojo.User;
import com.peterpig.codehome.pojo.UserExample;
import com.peterpig.codehome.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 用户服务层实现类
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User login(User user) {
        UserExample example=new UserExample();
        UserExample.Criteria criteria=example.createCriteria();
        criteria.andLoginNameEqualTo(user.getLoginName());
        List<User> resultUser=this.userMapper.selectByExample(example);
        if(resultUser!=null){
            boolean flag=this.comparePassword(user.getLoginPassword(),resultUser.get(0).getLoginPassword());
            if(flag){
                User u=resultUser.get(0);
                u.setLastLoginTime(new Date());
                this.userMapper.updateByPrimaryKey(u);
                return u;
            }
        }
        return null;
    }

    @Override
    public int register(User user) {
        int result=this.userMapper.insert(user);
        return result;
    }

    /**
     * 检查查询出来的密码和用户输入的密码是否相同
     * @param enterPassword 用户输入的密码
     * @param selectPassword 查询出来的密码
     * @return true为相同，false为不相同
     */
    private boolean comparePassword(String enterPassword,String selectPassword){
        if(enterPassword.equals(selectPassword))
            return true;
        else
            return false;
    }

    @Override
    public int updateNickname(Integer userId, String nickname) {
        //创建查询条件
        UserExample example=new UserExample();
        UserExample.Criteria criteria=example.createCriteria();
        criteria.andIdEqualTo(userId);
        User user=new User();
        user.setNickname(nickname);
        //执行修改，并且接受返回值
        int result=this.userMapper.updateByExampleSelective(user,example);
        return result;
    }
}
