package com.peterpig.codehome.service.impl;

import com.peterpig.codehome.mapper.PostMapper;
import com.peterpig.codehome.pojo.Post;
import com.peterpig.codehome.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

/**
 * @Author: jakciel
 * @Date: 2019/1/18 19:48
 * @Version 1.0
 */
@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostMapper postMapper;

    @Override
    public int newPost(Post post) {
        int result=this.postMapper.insert(post);
        return result;
    }

    @Override
    public int removePostById(Long postId) {
        int result=this.postMapper.deleteByPrimaryKey(postId);
        return result;
    }

    @Override
    public int updatePostInfo(Post post) {
        return 0;
    }

    @Override
    public List<Map> getPostPage(int page, int rows, String keyWord) {
        return null;
    }

    @Override
    public List<Map> getPostPage(String keyWord) {
        return null;
    }
}
