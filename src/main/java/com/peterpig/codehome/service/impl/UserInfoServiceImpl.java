package com.peterpig.codehome.service.impl;

import com.peterpig.codehome.mapper.UserInfoMapper;
import com.peterpig.codehome.pojo.UserInfo;
import com.peterpig.codehome.pojo.UserInfoExample;
import com.peterpig.codehome.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: jakciel
 * @Date: 2019/1/11 17:23
 * @Version 1.0
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public int addUserInfo(UserInfo userInfo) {
        int result=this.userInfoMapper.insert(userInfo);
        return result;
    }

    @Override
    public int updateUserInfo(UserInfo userInfo) {
        UserInfoExample example=new UserInfoExample();
        UserInfoExample.Criteria criteria=example.createCriteria();
        //创建修改条件，根据用户id去进行修改
        criteria.andUserIdEqualTo(userInfo.getUserId());
        //执行修改
        int result=this.userInfoMapper.updateByExampleSelective(userInfo,example);
        return result;
    }
}
