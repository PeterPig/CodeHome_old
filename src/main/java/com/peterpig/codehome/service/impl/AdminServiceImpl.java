package com.peterpig.codehome.service.impl;

import com.peterpig.codehome.mapper.AdminMapper;
import com.peterpig.codehome.pojo.Admin;
import com.peterpig.codehome.pojo.AdminExample;
import com.peterpig.codehome.service.AdminService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 管理员服务层接口实现类
 * @Author: Evan
 * @Date: 2019-1-11 15:41
 */
@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminMapper adminMapper;

    @Override
    public Admin login(Admin admin) {
        if (admin != null) {
            AdminExample example = new AdminExample();
            AdminExample.Criteria criteria = example.createCriteria();
            //前端传过来的登录名和数据库中的登录名比较
            criteria.andLoginNameEqualTo(admin.getLoginName());
            List<Admin> list = this.adminMapper.selectByExample(example);
            if (list != null && list.size() != 0){
                //查询到了用户
                Admin admin1 = list.get(0);
                if (checkPassword(admin.getLoginPassword(), admin1.getLoginPassword())) {
                    //登录成功，修改上次登录时间
                    admin1.setLastLoginTime(new Date());
                    this.adminMapper.updateByPrimaryKey(admin1);
                    return admin1;
                }
            }
        }
        return null;
    }

    @Override
    public Admin selectById(int id) {
        if (id > 0) {
            return this.adminMapper.selectByPrimaryKey(id);
        }
        return null;
    }

    @Override
    public int addAdmin(Admin admin) {
        if (admin != null) {
            int insert = this.adminMapper.insert(admin);
            return insert;
        }
        return 0;
    }

    @Override
    public int updateAdmin(Admin admin) {
        if (admin != null) {
            int result = this.adminMapper.updateByPrimaryKey(admin);
            return result;
        }
        return 0;
    }


    /**
     * 检查输入的密码和真实密码是否一致
     * @param enterPass 输入的密码
     * @param realPass 真实密码
     * @return true表示一致，false表示不一致
     */
    private boolean checkPassword(String enterPass, String realPass){
        return enterPass.equals(realPass);
    }

//    @Test
//    public void te(){
//        Admin admin = new Admin();
//        admin.setLoginName("123");
//        admin.setLoginPassword("123");
//        System.out.println(login(admin));
//    }

}
