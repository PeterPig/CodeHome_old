package com.peterpig.codehome.service.impl;

import com.peterpig.codehome.service.PictureService;
import com.peterpig.codehome.utils.FtpUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

/**
 * @Author: Evan
 * @Date: 2019-1-19 14:56
 */
@Service
public class PictureServiceImpl implements PictureService {

    @Value("${FTP_ADDRESS}")
    private String FTP_ADDRESS;
    @Value("${FTP_PORT}")
    private Integer FTP_PORT;
    @Value("${FTP_USERNAME}")
    private String FTP_USERNAME;
    @Value("${FTP_PASSWORD}")
    private String FTP_PASSWORD;
    @Value("${FTP_BASE_PATH}")
    private String FTP_BASE_PATH;
    @Value("${IMAGE_BASE_URL}")
    private String IMAGE_BASE_URL;

    @Override
    public Map uploadPicture(MultipartFile uploadFile) {
        Map resultMap = new HashMap();
        try {
            //生成一个新的文件名
            //取原始文件名
            String oldName = uploadFile.getOriginalFilename();
            //生成新文件名
            String newName = UUID.randomUUID().toString();
            //String newName = IDUtils.genImageName();
            newName = newName+oldName.substring(oldName.lastIndexOf("."));
            //图片上传
            String imagePath=new DateTime().toString("/yyyy/MM/dd");
            boolean result= FtpUtil.uploadFile(FTP_ADDRESS,FTP_PORT,FTP_USERNAME,FTP_PASSWORD,FTP_BASE_PATH,imagePath,newName,uploadFile.getInputStream());
            List list = new ArrayList();
            //返回结果
            if (!result){
                resultMap.put("errno", -1);
                resultMap.put("data", list);
                return resultMap;
            }
            list.add("http://localhost:8081/images" + imagePath + "/" + newName);
            resultMap.put("errno", 0);
            resultMap.put("data", list);
            return resultMap;
        } catch (IOException e) {
            resultMap.put("errno",-2);
            resultMap.put("data","上传发生异常");
            return resultMap;
        }
    }
}
