package com.peterpig.codehome.service;

import com.peterpig.codehome.pojo.User;

/**
 * 用户服务层接口
 */
public interface UserService {

    /**
     * 用户登录
     * @param user 页面传输过来的对象
     * @return 返回user对象
     */
    User login(User user);

    /**
     * 注册
     * @param user 注册的user对象
     * @return 注册成功返回true，注册失败返回false
     */
    int register(User user);

    /**
     * 根据用户id修改用户昵称
     * @param userId 用户id
     * @param nickname 进行修改的昵称名
     * @return 返回1修改成功，返回0修改失败
     */
    int updateNickname(Integer userId,String nickname);

}
