package com.peterpig.codehome.service;


import com.peterpig.codehome.pojo.UserInfo;

public interface UserInfoService {

    /**
     * 用户添加自己的详情信息
     * @param userInfo UserInfo对象
     * @return 添加成功返回1，添加失败返回0
     */
    int addUserInfo(UserInfo userInfo);

    /**
     * 修改用户详情信息
     * @param userInfo UserInfo对象
     * @return 修改成功返回1，修改失败返回0
     */
    int updateUserInfo(UserInfo userInfo);
}
