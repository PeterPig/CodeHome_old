package com.peterpig.codehome.service;

import com.peterpig.codehome.pojo.Post;

import java.util.List;
import java.util.Map;

/**
 * @Author: jakciel
 * @Date: 2019/1/15 13:36
 * @Version 1.0
 */
public interface PostService {

    /**
     * 用户发表帖子
     * @param post Post对象
     * @return 发帖成功返回1，发帖失败返回0
     */
    int newPost(Post post);


    /**
     *  根据帖子id删除帖子
     * @param postId 帖子id
     * @return 删除成功返回1，删除失败返回0
     */
    int removePostById(Long postId);

    /**
     * 修改帖子内容
     * @param post Post对象
     * @return 修改成功返回1，修改失败返回0
     */
    int updatePostInfo(Post post);

    /**
     * 分页查询
     * @param page 当前页
     * @param rows 每页的行数
     * @param keyWord 关键字
     * @return 返回post对象泛型的列表
     */
    List<Map> getPostPage(int page, int rows, String keyWord);

    /**
     * 根据关键字
     * @param keyWord
     * @return 返回post泛型的列表
     */
    List<Map> getPostPage(String keyWord);
}
