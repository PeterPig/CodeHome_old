package com.peterpig.codehome.service;

import com.peterpig.codehome.pojo.Language;

import java.util.List;

/**
 * 语种的服务层接口
 * @Author: Evan
 * @Date: 2019-1-14 17:59
 */
public interface LanguageService {
    /**
     * 添加语种
     * @param language 语种对象
     * @return 0表示失败，1表示成功
     */
    int addLang(Language language);

    /**
     * 添加语种
     * @param language 语种对象
     * @return 0表示失败，1表示成功
     */
    int updateLang(Language language);

    /**
     * 添加语种
     * @param id 删除的语种ID
     * @return 0表示失败，1表示成功
     */
    int deleteLang(int id);

    /**
     * 通过语种ID查询语种
     * @param id 语种ID
     * @return null表示未查询到
     */
    Language selectById(int id);

    /**
     * 查询所有语种
     * @return null表示未查询到
     */
    List<Language> selectAll();
}
