package com.peterpig.codehome.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @Author: Evan
 * @Date: 2019-1-19 14:55
 */
public interface PictureService {
    Map uploadPicture(MultipartFile uploadFile) ;
}
