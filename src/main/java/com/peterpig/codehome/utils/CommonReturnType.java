package com.peterpig.codehome.utils;

/**
 * @Author: jakciel
 * @Date: 2019/1/12 16:54
 * @Version 1.0
 */
public class CommonReturnType {

    private Integer result;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
}
