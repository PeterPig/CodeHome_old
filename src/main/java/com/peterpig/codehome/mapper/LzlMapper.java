package com.peterpig.codehome.mapper;

import com.peterpig.codehome.pojo.Lzl;
import com.peterpig.codehome.pojo.LzlExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LzlMapper {
    long countByExample(LzlExample example);

    int deleteByExample(LzlExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Lzl record);

    int insertSelective(Lzl record);

    List<Lzl> selectByExample(LzlExample example);

    Lzl selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Lzl record, @Param("example") LzlExample example);

    int updateByExample(@Param("record") Lzl record, @Param("example") LzlExample example);

    int updateByPrimaryKeySelective(Lzl record);

    int updateByPrimaryKey(Lzl record);
}