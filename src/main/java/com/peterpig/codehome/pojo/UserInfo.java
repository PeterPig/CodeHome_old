package com.peterpig.codehome.pojo;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class UserInfo {
    private Integer userId;

    private String realName;

    private Integer gender;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date bornDate;

    private String advantageLang;

    private String presentation;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Date getBornDate() {
        return bornDate;
    }

    public void setBornDate(Date bornDate) {
        this.bornDate = bornDate;
    }

    public String getAdvantageLang() {
        return advantageLang;
    }

    public void setAdvantageLang(String advantageLang) {
        this.advantageLang = advantageLang == null ? null : advantageLang.trim();
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation == null ? null : presentation.trim();
    }
}