package com.peterpig.codehome.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class UserInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andRealNameIsNull() {
            addCriterion("real_name is null");
            return (Criteria) this;
        }

        public Criteria andRealNameIsNotNull() {
            addCriterion("real_name is not null");
            return (Criteria) this;
        }

        public Criteria andRealNameEqualTo(String value) {
            addCriterion("real_name =", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameNotEqualTo(String value) {
            addCriterion("real_name <>", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameGreaterThan(String value) {
            addCriterion("real_name >", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameGreaterThanOrEqualTo(String value) {
            addCriterion("real_name >=", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameLessThan(String value) {
            addCriterion("real_name <", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameLessThanOrEqualTo(String value) {
            addCriterion("real_name <=", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameLike(String value) {
            addCriterion("real_name like", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameNotLike(String value) {
            addCriterion("real_name not like", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameIn(List<String> values) {
            addCriterion("real_name in", values, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameNotIn(List<String> values) {
            addCriterion("real_name not in", values, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameBetween(String value1, String value2) {
            addCriterion("real_name between", value1, value2, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameNotBetween(String value1, String value2) {
            addCriterion("real_name not between", value1, value2, "realName");
            return (Criteria) this;
        }

        public Criteria andGenderIsNull() {
            addCriterion("gender is null");
            return (Criteria) this;
        }

        public Criteria andGenderIsNotNull() {
            addCriterion("gender is not null");
            return (Criteria) this;
        }

        public Criteria andGenderEqualTo(Integer value) {
            addCriterion("gender =", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotEqualTo(Integer value) {
            addCriterion("gender <>", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThan(Integer value) {
            addCriterion("gender >", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThanOrEqualTo(Integer value) {
            addCriterion("gender >=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThan(Integer value) {
            addCriterion("gender <", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThanOrEqualTo(Integer value) {
            addCriterion("gender <=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderIn(List<Integer> values) {
            addCriterion("gender in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotIn(List<Integer> values) {
            addCriterion("gender not in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderBetween(Integer value1, Integer value2) {
            addCriterion("gender between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotBetween(Integer value1, Integer value2) {
            addCriterion("gender not between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andBornDateIsNull() {
            addCriterion("born_date is null");
            return (Criteria) this;
        }

        public Criteria andBornDateIsNotNull() {
            addCriterion("born_date is not null");
            return (Criteria) this;
        }

        public Criteria andBornDateEqualTo(Date value) {
            addCriterionForJDBCDate("born_date =", value, "bornDate");
            return (Criteria) this;
        }

        public Criteria andBornDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("born_date <>", value, "bornDate");
            return (Criteria) this;
        }

        public Criteria andBornDateGreaterThan(Date value) {
            addCriterionForJDBCDate("born_date >", value, "bornDate");
            return (Criteria) this;
        }

        public Criteria andBornDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("born_date >=", value, "bornDate");
            return (Criteria) this;
        }

        public Criteria andBornDateLessThan(Date value) {
            addCriterionForJDBCDate("born_date <", value, "bornDate");
            return (Criteria) this;
        }

        public Criteria andBornDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("born_date <=", value, "bornDate");
            return (Criteria) this;
        }

        public Criteria andBornDateIn(List<Date> values) {
            addCriterionForJDBCDate("born_date in", values, "bornDate");
            return (Criteria) this;
        }

        public Criteria andBornDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("born_date not in", values, "bornDate");
            return (Criteria) this;
        }

        public Criteria andBornDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("born_date between", value1, value2, "bornDate");
            return (Criteria) this;
        }

        public Criteria andBornDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("born_date not between", value1, value2, "bornDate");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangIsNull() {
            addCriterion("advantage_lang is null");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangIsNotNull() {
            addCriterion("advantage_lang is not null");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangEqualTo(String value) {
            addCriterion("advantage_lang =", value, "advantageLang");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangNotEqualTo(String value) {
            addCriterion("advantage_lang <>", value, "advantageLang");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangGreaterThan(String value) {
            addCriterion("advantage_lang >", value, "advantageLang");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangGreaterThanOrEqualTo(String value) {
            addCriterion("advantage_lang >=", value, "advantageLang");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangLessThan(String value) {
            addCriterion("advantage_lang <", value, "advantageLang");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangLessThanOrEqualTo(String value) {
            addCriterion("advantage_lang <=", value, "advantageLang");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangLike(String value) {
            addCriterion("advantage_lang like", value, "advantageLang");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangNotLike(String value) {
            addCriterion("advantage_lang not like", value, "advantageLang");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangIn(List<String> values) {
            addCriterion("advantage_lang in", values, "advantageLang");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangNotIn(List<String> values) {
            addCriterion("advantage_lang not in", values, "advantageLang");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangBetween(String value1, String value2) {
            addCriterion("advantage_lang between", value1, value2, "advantageLang");
            return (Criteria) this;
        }

        public Criteria andAdvantageLangNotBetween(String value1, String value2) {
            addCriterion("advantage_lang not between", value1, value2, "advantageLang");
            return (Criteria) this;
        }

        public Criteria andPresentationIsNull() {
            addCriterion("presentation is null");
            return (Criteria) this;
        }

        public Criteria andPresentationIsNotNull() {
            addCriterion("presentation is not null");
            return (Criteria) this;
        }

        public Criteria andPresentationEqualTo(String value) {
            addCriterion("presentation =", value, "presentation");
            return (Criteria) this;
        }

        public Criteria andPresentationNotEqualTo(String value) {
            addCriterion("presentation <>", value, "presentation");
            return (Criteria) this;
        }

        public Criteria andPresentationGreaterThan(String value) {
            addCriterion("presentation >", value, "presentation");
            return (Criteria) this;
        }

        public Criteria andPresentationGreaterThanOrEqualTo(String value) {
            addCriterion("presentation >=", value, "presentation");
            return (Criteria) this;
        }

        public Criteria andPresentationLessThan(String value) {
            addCriterion("presentation <", value, "presentation");
            return (Criteria) this;
        }

        public Criteria andPresentationLessThanOrEqualTo(String value) {
            addCriterion("presentation <=", value, "presentation");
            return (Criteria) this;
        }

        public Criteria andPresentationLike(String value) {
            addCriterion("presentation like", value, "presentation");
            return (Criteria) this;
        }

        public Criteria andPresentationNotLike(String value) {
            addCriterion("presentation not like", value, "presentation");
            return (Criteria) this;
        }

        public Criteria andPresentationIn(List<String> values) {
            addCriterion("presentation in", values, "presentation");
            return (Criteria) this;
        }

        public Criteria andPresentationNotIn(List<String> values) {
            addCriterion("presentation not in", values, "presentation");
            return (Criteria) this;
        }

        public Criteria andPresentationBetween(String value1, String value2) {
            addCriterion("presentation between", value1, value2, "presentation");
            return (Criteria) this;
        }

        public Criteria andPresentationNotBetween(String value1, String value2) {
            addCriterion("presentation not between", value1, value2, "presentation");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}