package com.peterpig.codehome.pojo;

import java.util.Date;

public class Lzl {
    private Integer id;

    private String content;

    private Integer replyId;

    private Integer lzlId;

    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getReplyId() {
        return replyId;
    }

    public void setReplyId(Integer replyId) {
        this.replyId = replyId;
    }

    public Integer getLzlId() {
        return lzlId;
    }

    public void setLzlId(Integer lzlId) {
        this.lzlId = lzlId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}