# CodeHome（即将重构）
# 项目介绍
&nbsp;&nbsp;&nbsp;&nbsp;程序员之家为程序员们提供了一个交流平台，只需要把你在开发中碰到的问题截图或是复制代码到编辑框，再发布，就能在平台上看到你的问题，不一会儿就会有人来和你交流，或许他和你碰到一样的问题，或许他知道如何解决。
# 技术选择
## 后端
SpringBoot、SpringFramework、SpringMVC、MyBatis-plus、Apache Shiro、Druid、Solr、Log4J、Maven。
## 前端
待定
# 架构图
制作中
# 环境搭建
## 开发工具

- MySQL
- Tomcat
- Git
- IntelliJ IDEA
- Navicat for Premium

## 开发环境

- Jdk 8
- MySQL 5.5
- Redis

# 开发规范约定
待定